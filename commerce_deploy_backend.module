<?php

/**
 * @file
 * Module file for Commerce deploy backend
 */

/**
 * Implements hook_preprocess_html().
 */
function commerce_deploy_backend_preprocess_html(&$variables) {
  $module_path = drupal_get_path('module', 'commerce_deploy_backend');
  drupal_add_css($module_path . '/theme/commerce-deploy-backend.css');
  drupal_add_js($module_path. '/theme/commerce-deploy-backend.js');
}

/**
 * Implements hook_navbar_alter().
 */
function commerce_deploy_backend_navbar_alter(&$items) {
  // Cycle through the administrative menu.
  // @todo: Creare a way to check what's been overridden via hook_navbar().
  if (isset($items['administration'])) {
    $administration_menu =& $items['administration']['tray']['navbar_administration']['administration_menu'];

    foreach ($administration_menu as $key => $item) {
      if (!isset($item['#href'])) {
        continue;
      }

      // Bring Content up a level.
      if ($item['#href'] == 'admin/commerce') {
        unset($administration_menu[$key]);
      }
    }
  }
}

/**
 * Implements hook_navbar().
 */
function commerce_deploy_backend_navbar() {
  if (user_access('access administration pages')) {
    // Menu tree all data doesn't work as expected, so we just do this.
    $commerce_root = menu_link_get_preferred('admin/commerce', 'management');
    $parameters = array(
      'active_trail' => array($commerce_root['plid']),
      'only_active_trail' => FALSE,
      'min_depth' => $commerce_root['depth']+1,
      'max_depth' => $commerce_root['depth']+1,
      'conditions' => array('plid' => $commerce_root['mlid']),
    );

    $commerce_menu_tree = menu_build_tree($commerce_root['menu_name'], $parameters);

    // Add attributes to the links before rendering.
    //navbar_menu_navigation_links($commerce_menu_tree);
    foreach ($commerce_menu_tree as $key => $item) {
      $commerce_menu_tree[$key]['link']['localized_options']['attributes']['class'][] = 'navbar-menu-item';
    }

    $commerce_menu = array(
      '#heading' => t('Store menu'),
      'navbar_administration' => array(
        '#type' => 'container',
        '#attributes' => array(
          'class' => array('navbar-menu-commerce'),
        ),
        'commerce_menu' => menu_tree_output($commerce_menu_tree),
      ),
    );

    $items['commerce'] = array(
      '#type' => 'navbar_item',
      'tab' => array(
        '#type' => 'link',
        '#title' => t('Store'),
        '#href' => 'admin/commerce',
        '#options' => array(
          'attributes' => array(
            'title' => t('Store menu'),
            'class' => array('navbar-icon', 'navbar-icon-commerce'),
          ),
        ),
      ),
      'tray' => $commerce_menu,
      '#weight' => 10,
    );

    return $items;
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Ensure Coupon and Discount reference fields show below the fold, and more.
 */

function commerce_deploy_backend_form_commerce_order_ui_order_form_alter(&$form, &$form_state) {
  // If Commerce Coupon is enabled, move it to bottom.
  if (module_exists('commerce_coupon')) {
    $form['commerce_coupons']['#weight'] = 10;
  }
  // If Commerce Discount is enabled, move it to bottom.
  if (module_exists('commerce_discount')) {
    $form['commerce_discounts']['#weight'] = 10;
  }
}

/**
 * Implements hook_views_api().
 */
function commerce_deploy_backend_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'commerce_deploy_backend') . '/includes/views',
  );
}

/**
 * Implements hook_preprocess_views_view().
 */
function commerce_deploy_backend_preprocess_views_view($vars){
  $view = $vars['view'];

  if ($view-> name == 'commerce_print_orders' && !isset($view->preview)) {
     drupal_add_js('window.print();', 'inline');
  }
}

/**
 * Implements hook_views_default_views_alter().
 */
function commerce_deploy_backend_views_default_views_alter(&$views) {
  if (isset($views['commerce_orders'])) {
    $handler=& $views['commerce_orders']->display['default']->handler;
    /* Field: Bulk operations: Commerce Order */
    $vbo['views_bulk_operations']['id'] = 'views_bulk_operations';
    $vbo['views_bulk_operations']['table'] = 'commerce_order';
    $vbo['views_bulk_operations']['field'] = 'views_bulk_operations';
    $vbo['views_bulk_operations']['vbo_settings']['display_type'] = '0';
    $vbo['views_bulk_operations']['vbo_settings']['enable_select_all_pages'] = 1;
    $vbo['views_bulk_operations']['vbo_settings']['force_single'] = 0;
    $vbo['views_bulk_operations']['vbo_settings']['entity_load_capacity'] = '10';
    $vbo['views_bulk_operations']['vbo_operations'] = array(
      'action::views_bulk_operations_delete_item' => array(
        'selected' => 1,
        'postpone_processing' => 0,
        'skip_confirmation' => 0,
        'override_label' => 0,
        'label' => '',
      ),
      'action::views_bulk_operations_delete_revision' => array(
        'selected' => 0,
        'postpone_processing' => 0,
        'skip_confirmation' => 0,
        'override_label' => 0,
        'label' => '',
      ),
      'action::views_bulk_operations_script_action' => array(
        'selected' => 0,
        'postpone_processing' => 0,
        'skip_confirmation' => 0,
        'override_label' => 0,
        'label' => '',
      ),
      'action::views_bulk_operations_modify_action' => array(
        'selected' => 0,
        'postpone_processing' => 0,
        'skip_confirmation' => 0,
        'override_label' => 0,
        'label' => '',
        'settings' => array(
          'show_all_tokens' => 1,
          'display_values' => array(
            '_all_' => '_all_',
          ),
        ),
      ),
      'action::views_bulk_operations_argument_selector_action' => array(
        'selected' => 0,
        'skip_confirmation' => 0,
        'override_label' => 0,
        'label' => '',
        'settings' => array(
          'url' => '',
        ),
      ),
      'action::commerce_deploy_backend_view_print' => array(
        'selected' => 1,
        'skip_confirmation' => 1,
        'override_label' => 0,
        'label' => '',
      ),
      'action::system_send_email_action' => array(
        'selected' => 0,
        'postpone_processing' => 0,
        'skip_confirmation' => 0,
        'override_label' => 0,
        'label' => '',
      ),
    );
    $handler->display->display_options['fields'] = array_merge($vbo, $handler->display->display_options['fields']);
  }
}

/**
 * Implements hook_action_info().
 */
function commerce_deploy_backend_action_info() {
  return array(
    'commerce_deploy_backend_view_print' => array(
      'type' => 'commerce_order',
      'label' => t('Print orders'),
      'aggregate' => TRUE,
      'configurable' => FALSE,
      'hooks' => array(),
      'triggers' => array('any'),
    ),
  );
}

/**
 * Views bulk operation action callback.
 */
function commerce_deploy_backend_view_print($commerce_orders, $context = array()) {
  //View path
  $base_url = 'admin/commerce/orders/print/';
  //Pass contextual filter
  $arguments = implode(',', array_keys($commerce_orders));
  //Send it off
  drupal_goto($base_url . $arguments);
}
